# **Le Markdown et le Git**

## **Le Markdown**

Le Markdown n'est pas un language de programmation,  
c'est un language de prise de note ou de structure.

Le Markdown fonctionne comme le HTML, mais en version simplifiée.
- Quelques exemples de syntaxe :
 
    - ' # ' = < h1 > < /h1 > (le nombre de # correspond au énième h1, h2, etc...)  
    - ' - ' = < li > (pour creer une liste)  
    - ' (tab)- ' = < ul > (pour creer une sous liste)  
    - ' double space ' = < br > ( pour le retour a la ligne)  
    - ' ** (text) **' = pour du texte en **gras**  
    - ' * (text) * ' = pour du texte en *italic*  
    - ' *** (text) *** ' = pour du texte en *italic* et en **gras**

## **Le Git**

Installer Git : https://git-scm.com/

Git est un protocole qui permet de stocker des données pas forcément informatique.  
GitHub et GitLab sont deux sites qui utilise ce protocole.

GitLab permet d'envoyer et de recupérer les données envoyer sur GitLab rapidement.

## **Les Backticks**

Les Backticks permet de mettre du texte en surbrillance ou bien d'ecrire des  
extraits de code tel qu'il serait en vrai.

### Backtick simple
Pour l'utiliser on met une backtick avant la phrase et une a la fin de celle-ci.
AltGr + 7 = backtick sur Windows  
`cette phrase est importante`

### Backtick triple
Pour l'utiliser on met 3 backticks avant le code en question et 3 a la fin.
Apres les 3 backticks on choisit le type de code (JS, C , etc...).
```javascript
<!DOCTYPE html>

<html>
<head>
<title>Title of the document</title>
</head>
<body>
The content of the document
</body>
</html>
``` 
## **Structure d'une page**
La structure d'une page doit etre organiser en plusieur partie elle meme avec des sous partie.
- Une page internet doit comporter uniquement un seul **H1** le titre le plus important de la page qui va porter sur le sujet de la page.
- Les balise **H2** **H3** **H4** ont une ordre d'importance qu'il faut respecter.
- Organiser les differentes partie avec leurs contenue et leurs sujets (comme une dissertation).
L'organisation est super importante car elle permet un meilleur SEO (Search Engine Optimization), meilleur référencement sur internet.

## **Envoyer sur GitLab**

### Initialiser sont lien GitLab sur WebStorm

- Aller sur le site `Gitlab.com`, se connecter et recupérer sont lien HTTPS GitLab.
![etape1](Git%20etape%203.PNG)  

- Aller sur la page d'accueil de WebStorm et aller dans `Configure`, puis cliquer sur `Settings Repository`.
![etape2](Git%20etape%201.PNG)  
- Coller son lien GitLab, et cliquer sur `Merge`  
![etape3](Git%20etape%202.PNG)  

### Envoyer sur GitLab

-  Pour envoyer un fichier ou dossier sur GitLab on effectue un `commit`, le commit permet de choisir les fichiers, dossiers et autres a selectionner pour l'envoie
![commit](Commit.PNG)  
Cliquer sur la fleche verte et ne pas oublier de bien nommer son commit (penser a changer le nom du `commit` si on repush plusieurs versions du fichier)

- Suite au `commit` on `push` qui permet de valider l'envoie vers GitLab  
![push](Push.PNG)  
On peux voir le nom du `commit` que l'on a mis et ce que l'on veux `push`

