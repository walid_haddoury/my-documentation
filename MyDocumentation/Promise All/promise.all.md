# Promise All

## Faire plusieurs `.then` en meme temps 
![promise.all-V1](assets/PromiseAll.PNG)
Ici on a creer 2 `.get` que l'on a mis dans des `const`
Le `Promise.All` permet de jouer toutes les promesse en meme temps et de les affichers en arrays.
Ici on a besoin que d'un seul `.catch`, celui qui est dans le `Promise.All` car c'est la fonction la plus importante

## Utiliser `for` avec `Promise.All`

## Type de fonctions
### Fonctions Synchrone
Une fonction synchrone est du code qui va se jouer tout de suite dans delais, il n'y a pas de `.then`, la fonction repond tout de suite 
### Fonction Asynchrone
Une fonction Asynchrone a deux type :
- le code sera jouer de suite
- le code sera jouer avec du delais (car il faut faire la liaison avec le serveur qui est loin, pour ecrire dedant et pour lire)


## Rappel sur FireStore
- Pour initialiser firestore
````javascript
const db = admin.firestore();
````

- Pour recuperer la clé de service
````javascript
let serviceAccount = require('./key.json');
````

- Pour utliser la clé de service pour initialiser firestore
````javascript
admin.initializeApp({
credential: admin.credential.cert(serviceAccount)
});
````

````javascript
export function addHostel(hostel) {
  return db  // db veux dire que l'on va travailler dans la db
    .collection('hostels')  // pour dire dans quel collection on va travailler
    .doc() // pour dire dans quel doc on veux travailler, permet de cibler ce que l'on veux dans collection   
    .add(hostel) // verbe d'action qui se declenche apres '.doc' ou '.collection'
}
````