# Creer des promesses

Jusqu'a present on a travailler avec des promesse deja faite on a juste eu a les utiliser. Mais il est possible d'en creer aussi.

Il y a un outils qui permet de creer des promesses.
## Comment creer une promesse

- Pour creer une `promesse`, on creer une const dans la laquelle on va stocker notre promesse
- `new Promise` est une API qui a ete creer pour standardisé les promesses
- `new Promise` revoie une fonction de CallBack 
- `resolve` et `reject` sont des fonctions est non pas juste des noms (comme dans .map(value) ou l'on peux se que l'on veux a la place de value)
- `resolve` permet de mettre fin a la promesse et de donner la valeur finale de la promesse
- `reject` permet de rejeter la promesse et du coup c'est ce que l'on va recuperer dans le `catch`
- `setTimeout` permet de creer un delais entre le debut du code et la reponse, c'est en ms
Lorsque l'on fait une promesse on ne doit pas oublier la cas `reject` car on doit pouvoir catch les erreurs 
   
````javascript
const myPromise = new Promise((resolve, reject) => { // on va creer une const dans laquelle on va stocker `new Promise`
const random = Math.floor(Math.random() * 10 + 1);
    if (random <= 5){
return setTimeout(() => resolve(random), 1000); // le `setTimeout` permet de faire un appel differer et donc d'avoir un delais avant la reponse ici c'est 1000 ms on peux mettre autant que l'on veux mais c'est en ms
    }
else {
    return setTimeout(() => reject(random), random * 1000);
    }
    myPromise.then((random) => 
    {
        console.log(random)
        return random;
    })
    .then((random) => res.send(random + ''))
    .catch(e => res.status(500).send(e + ''))
})
````