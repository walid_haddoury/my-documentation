666# Installation d'un nouveau projet

## Installation de `Firebase`:

1. Creer un projet sur Firebase
2. Creer un Dossier du nom de notre Projet: (ex: Louis-Vuitton)
3. Creer un Dossier du nom de la Cloud Function que l'on va creer (par convention elle se nomme `doPostPicture`) 
4. Entrer via le Terminal dans notre Dossier, puis dans le dossier de notre Cloud Functions.
5. Taper `firebase init functions` et choisir nos options.
6. Add sur Git tous les fichiers

## Installation des `npm`

Pour installer les extentions npm dont on a besoin: 
- Aller dans le dossier `src`
- faire `npm i -S` pour les installer dans les `dependencies`
- faire `npm i -D` pour installer dans les `devDependencies`
ici on a besoin de `cors` et `express`

## Set up de la key du projet

Dans un projet il faut import la `key` firebase du projet dans tous les dossiers de nos functions
Creer un fichier `key-firebase.ts`
````typescript
export const key = {
    type: "service_account",
    project_id: "projet-louis-vuitton-promo2",
    private_key_id: "e4feeda743369d6a066aa9d40e38e4dc86501cb1",
    private_key: "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCu0WZxsNnsSbUK\n/pwGmwBhdoiTrAQZ4OcKZ9wBK3PkN4pPdrZ5RjeNB3nPNftaoER2cuA0kMtImcru\nPMsLKBQCa84pd0/qWLb8dA+6pgye7ab9DDvVcw2/RlLlrBM8uhcstZz2Z4k6K4An\nDmUyShMcdGRuG4LdlATl7NNcK5QLdNnlTNw6qQbd1Cxmji1uX4hnPCjwLyOIElK1\nXRvoiYL3K6Mo5P65dOSgjpg/QH8XLDWKVmilCPDwg5UyJRiwY1jpyFBtrIBKk9ef\nDrvzv2YZCKxZJq27uwHfbzt+qD99A6O0naFdlwPo1PMdukEdNmUMrm6n/rQc4sRm\nIkUxf1xVAgMBAAECggEAIosyKQj/JtextIh3ID8Epj2b40I8VTjBrqazLG//qHrp\nE6ari+86Dt4JTc6m6lN888LjVr+9y4zIMqdEn+G9gnT2+e6flfkNv3jXmtQw7j6b\nnqkKMbVYZBFrVALakkdbSPRiHbZ7wUjTO+R2TqegeyDsr200QMB870oKwc3CpTiQ\nKD1g1ce0K3y5dYDgg4c/QrbGScWX79+CM0k2su4Ftz+JRHQrw7RycRa0jZyH2X5k\npA0ic8GcDvPYXtcTfvlBkDRakSsTFhDeFhwNHIq0NQddXYl4C+8Zl++6LqMZfPEo\n1h4ifiFUrS1hJZFb/IbXW0D/k6pCrRqmMcuoaZpliQKBgQDaXFqFFNJabGd3yzyK\nk1XrMV2MFuDulI59Di7G0Khde7YAqISa7kwFYtBtFfmwUmVY3OoRc+s9GnQQ1i5M\nIo23HBdFWlxBaWqBod3wQGwvXhuhkUDOZCb9KaZwURgh8gGNDV/LCUWkucLIESQQ\nlxAA0InerhfS12cKafwlVPHhcwKBgQDM86AzlNjPQFeI3+iMHS/o2EkazUnnMXC8\npnD91T70RgVNBftoqLgSLHFQ/dgJXI2mWGJeDbq+71OSso9xiiSJYWb5zq9TiOCB\nnkKkY6mMDjz6PJf+T6XwF0uUIr59TM3dayBo7RdQ5q2etvrRt6Iu5egD7JTLie9E\nPGYhVsi5FwKBgQCFI2BR2lMzrPsdUH7WY2U0pQHLw7tb3JpQZ5BfyqONdDuRhS5t\nJLSXaBt7F7YJKwP+PZw+Dfa1Z+ayyVSrOTo9FlSeBa1QeMzmw2IF9dHAw9mHFoB0\nFx6qp3WC7/8N23fUn/3zVvDushCwWbsqnmC0jnIxouaFuK5r7BlATd25PwKBgGS9\nFzBjaFIuiRghHrfIHOVxcjV3VG0MEyLXGNDbgU8mH7LlTtb5n0hniedKbmbjlEiA\nRxF95B23mzePBwzC3wSb2LbPDcSckeWEObN9vTe3Q+en4vnAgorJmH+euEz0dHfy\nEQMiDoTYbdyGijCM923f2bQcFGMbUH0Uxwp6Er1PAoGAYnynwUdvKWbXaXXEDzX4\n4O+KFg6Dg7RiJJEk6UlTVMNpsAp5mWyk8KOQPNXU6OugTKzMM8humBpqc9lMfknU\nLuT49wH6ALcfFyfB8U0NcMc/M9l/HDrp2LV87QBBIGq0q1KJnCvdnrfrS5fE4I+R\ncipW2XzBvrvBlX/Kz55tD4Y=\n-----END PRIVATE KEY-----\n",
    client_email: "firebase-adminsdk-90ou2@projet-louis-vuitton-promo2.iam.gserviceaccount.com",
    client_id: "100534889126091042810",
    auth_uri: "https://accounts.google.com/o/oauth2/auth",
    token_uri: "https://oauth2.googleapis.com/token",
    auth_provider_x509_cert_url: "https://www.googleapis.com/oauth2/v1/certs",
    client_x509_cert_url: "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-90ou2%40projet-louis-vuitton-promo2.iam.gserviceaccount.com"
};
````
On peux par la suite import la key partout dans le dossier de la Cloud Functions, il faut le faire dans toutes les Cloud Functions.

````typescript
import * as admin from 'firebase-admin';
import {key} from "./key";

admin.initializeApp({
    credential: admin.credential.cert(key as any)
});
const db = admin.firestore();
````

## Set up des import pour `express` et `cors`

Pour utiliser `cors` et `express`:
````typescript
import * as functions from 'firebase-functions';
import * as express from 'express';

const cors = require('cors');

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());
````

### Tips
Convention des noms dans le back:
- On commence tout les noms de nos functions par `do`+ ce qu'elle fait.
- Les action qui s'effectue lors de l'appel de la Cloud Functions s'appelle des:
    - `onCreate` a la creation d'une data
    - `onDelete` a la supression d'une data
    - `onWrite` 
    - `onUpDate` a la mise a jour d'une data

- Installer firebase dans chaque Cloud functions que l'on va  creer.

#### A faire quand le projet est Set up
- Aller dans `package.json` et modifier le `deploy`:
````.json
"deploy:doGetPictures": "firebase deploy --only functions:doGetPictures",
```` 
Changer le nom du deploy avec le nom de la Cloud Function
- Faire pareil avec le `serve`:
````json
"serve:doCreateOwner": "npm run build && firebase serve --only functions:doCreateOwner",
````
- Ajouter dans le `package.json` le `watch`
````json
"watch": "tsc -w",
````
dupliquer la commande `build` et ajouter `-w`