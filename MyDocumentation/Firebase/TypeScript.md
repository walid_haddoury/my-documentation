# TypeScript

Il y a plusieurs façon de travailler avec  `TypeScript` :
- La premiere est de parametrer entierement TypeScript, une fois fini TypeScript va fonctionner tout seul.
- Nous on va utiliser les fonction de FireBase pour faire du `BackOffice` en `ServeurLess`

Notre code que l'on va ecrire en TypeScript va se transpiller dans un fichier: 
- aller dans tsSrc, functions, lib, puis ouvrir le fichier index.js ou le code est transpiller en JS (le code ressemble beaucoup au TypeScript)

## Les debut de TypeScript :

![exemple1](assets/TypeScript-exemple1.PNG)  
Les deux ligne veulent dire la meme chose, la premiere ligne la syntaxe est en TypeScript et la deuxieme en JS Node.
 
![exemple2](assets/TypeScript-exemple2.PNG)  
Cette ligne permet de demander au fonctions de firebase de creer un lien HTTPS (le `S` pour sécurisé), qui va afficher sur la page `Hello from Firebase!`
Le `resquest` = req en node
Le `response` = res en node  
![exemple3](assets/TypeScript-exemple3.PNG)  
Le nom de la const va s'afficher dans le lien que va nous creer `Firebase` (ici c'est test)  

## Les `import` et `export`

### les `export`
Pour commencer on creer un dossier `models` dans `src`: 
- On creer ensuite a l'interieur de ce dossier `models`, un fichier `hotel.model.ts` (le nom est tres important ici on le nomme comme ça pour dire que le fichier contient des models d'hotels), et un deuxieme fichier `room.model.ts`
-Ensuite on ecrit :  
![exmple export typescript](assets/exemple-export-typescript.PNG)  
On doit typé toutes les informations que l'on met.
exemple: 
id: number; (on doit lui dire l'information est de quel type)

-Une fois les models fini on doit creer une fichier `database.data.ts` dans `src`
- Dans `database.data.ts` on ajoute le tableau avec tout les `hostels`  
![exemple database](assets/exemple-database-typescript.PNG)

-Lors du typage on peux avoir des erreurs si l'on retourne un mauvais resultat, on peux donc dire que nos `clé` et leur contenu sont optionnel  
![exemple optional typage](assets/export-optional-typescript.PNG)  
le `?` veux dire que c'est optionnel.

### les `import`
- Pour importer une data ou autre..., il faut passer le cursseur sur l'objet en question et faire `ALT + ENTRER` et l'`import` ce fera automatiquement
![exemple import](assets/exemple-database-typescript.PNG) 

## `.then`

- `.then` est une promesse 

`````javascript
app.get('/', function(req, res) {
   db.collection('hostels')
        .doc('test')
        .set({tata: 15})
        .then(() => res.send('ok')); // Une fois le code au dessus de then est jouer on peux faire les operation de then (ici envoyer ok)
        .catch(e => res.status(500).send(e));
});
`````
- `.then` permet de `catch` les erreurs

## Operateurs `get, post, put, delete`
- delete = `.delete`
- get = `.get`
- post = `.add`
- put = `set`
- patch = `.update`

### Rappel :

Ne pas oublier de tout typé dans nos focntions sinon ça ne merchera pas.