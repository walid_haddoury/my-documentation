# FireBase Install

## Etape d'installation de firebase:

- Creer un dossier dans `tryHard` qui s'appel `tsSrc`
- Ouvrir le terminal et regqrder si l'on n'est dans `tryHard` (Pour regarder sont chemin faire `pwd` sur Mac)
![etape1](assets/Firebase-Install-terminal.PNG)
- Faire `cd tsSrc` pour entrer dans le dossier `tsSrc` (dans le terminal)
- Faire `firebase init functions` (dans le terminal)
    - faire `Y` pour la question pret a demarrer
    - faire `use an existing project`, puis choisir le projet sur le CV qu'on a deployer
    - chosir `TypeSCript`
    - faire `Y` pour choisir `TSLint` (qui permet de corriger le code)
    - faire `Y` pour installer toutes les appications pour faire marcher `FireBase` et `TypeScript`
- Faire clique droit sur `tsSrc` puis cliquer sur `Synchronize`
- Add sur GitLab tout les fichiers qui sont en rouge
Si on veux coder c'est dans le dossier `src`

## Les Script de `package.json`

### A quoi sert les `Script` ? 
Les scripts vont permettre d'avoir des raccourcis, et de lancer des commandes automatiquement. 

### Installation des Script de `package.json`
- Faire clique droit sur `package.json`
- Cliquer sur `Show npm Srcipts`, une fenetre va s'ouvrir avec le nom des commandes, si on veux les utiliser on n'a juste a cliquer dessus.

### Commande `watch`
-Faire CTRL + D (pour dupliquer la ligne) de la ligne `build` dans le `package.json` dans `src`
- modifier la ligne `build`  
![exemple watch](assets/firebase-watch-exemple.PNG)  
ensuite faire `synchronize` les `npm`, puis cliquer sur commande `watch`

### Installer `express`
- Aller dans le terminal et faire `cd functions`
- faire dans le terminal `npm i -S express`

-Puis aller dans `index.ts` puis ajouter :
````javascript
import * as express from "express";

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));
````