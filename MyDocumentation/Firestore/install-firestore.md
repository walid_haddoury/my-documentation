# Firestore

## Installation de la base de donnée sur firebase
### Etapes :
- Aller sur firebase, cliquer sur le projet en question
- Puis cliquer sur `Accéder a la documentation` en haut a droite
- Puis aller sur `Get started for Admin`
- Aller sur `Cloud Functions`
    - `Get started`
        - `Test Functions`
            - `Run functions locally`
   
- Aller sur  https://console.cloud.google.com/iam-admin/serviceaccounts?authuser=0
    - `Sélectionnez un projet`
        - Choisir le projet (ici `CVenLigne`)
            - Selectionner `App Engine default service account`
                - Aller sur le menu a droit et faire `Creer une clé`
                    - Choisir `JSON`
                        - Une fois la clé telecharger, la mettre dans `functions`
                        
- Retourner sur `Run functions locally`
    - entrer dans le terminal la commande `set GOOGLE_APPLICATION_CREDENTIALS=(chemin de la clé)` (bien prendre la commande pour windows)

- Aller sur la categorie `Cloud Firestore`
    - Voir `Initialize Cloud Firestore`
        - Choisir `Node.js`
        
- Aller sur le fichier `index.ts`
    - Ajouter `let serviceAccount = require('chemin a Key.json');`
        - faire une copie de la clé firestore et la coller dans `src`
    - Ajouter  
````typescript
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});
````  
    - Ajouter `import * as admin from 'firebase-admin';`
    - Ajouter `const db = admin.firestore();`
    - Ajouter
````typescript
db // active la base de donnée
    .collection('hostels') // va dans la collection hostels qu'on a creer 
    .doc('test') // va dans le document test qu'on a creer 
    .set({tata: 12}); // met une valeur, ici tata qu'on a creer 
````

    
- Retourner sur le projet firebase
    - Aller dans `Database`
        - `Creer une base de données`
            - `Commencer en mode test`
                - Choisir l'europe
                

- Aller dans `Cloud functions`
    - `Test functions`
        - `Run functions locally`
        - Voir `Run the emulator suite`
            - copier `firebase emulators:start`
-Aller dans `package.json`
    - Ajouter `"emulator": "firebase emulators:start"` permet de pouvoir lire la clé qu'on a entrer 


`emulator` permet de travailler sans affecter la base de donné, donc si on veux travailler en production on fait `serve`