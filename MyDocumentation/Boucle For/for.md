# La boucle `for`

- La boucle `for` est un outils qui permet de faire des boucles
- Le but d'une boucle `for` est de generer du contenu
- La boucle `for` est distinct en deux partie,
````javascript
for (/* le conditionnel */) {
// pour executer ce qu'il y a dans la boucle for
 }
````

- La boucle `for` est composer en trois partie
````javascript
for (------ ; ------ ; ------){

}
````
On peux executer une boucle `for` sans l'une de ces trois partie !! 

````javascript
for (/*partie initialisation*/ ; /*condition*/ ; /*code repeter*/) {

}
````
1. La partie initialisation est un expression de code qui est evalué au demarrage de la boucle `for` (en gros dans cette partie le code va etre executer une seul fois)
2. La condition dans laquelle on joue la boucle `for` (predicat), on peux mettre `true` ou `false` mais dans le cas `true` on aura une boucle infinie et le cas `false` une boucle qui ne commencera jamais, on peux mettre n'importe quel condition meme si elle n'a pas de rapport avec la boucle `for`
3. Morceau de code qui sera jouer lors de chaque boucle, n peux y mettrece que l'on veux 

## Syntaxe 
````javascript
i++; // veux dire i = i + 1 ; on peux le faire aussi avec ' - '
````
````javascript
i+=5; // veux dire i = i + 5; on peux le faire aussi avec ' - ', ' * ', ' / '
````