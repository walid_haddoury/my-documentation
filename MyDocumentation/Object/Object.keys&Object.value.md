# Object.value & Object.keys
##Pourquoi utiliser Object ?

- `Object.keys` ou `Object.value` permet de modifier un objet en tableau. On peux aussi choisir un element de l'Object pour le transfomer en tableau.
exemple :

````javascript
const my= {
    tata : 0,
    toto : 0,
    tato : 1,
    totu : 2,
};

Object.keys(my);
````
ici Keys correspond a tata, toto, etc...

````javascript
const my= {
    tata : 0,
    toto : 0,
    tato : 1,
    totu : 2,
};
Object.values(my);
````
ici Values correspond a 0, 0, 1, etc...

- Une fois qu'on a modifier l'object en tableau on peux lui appliquer `.sort`, `.map`, etc...
- On ne peux pas manipuler les objects comme les tableaux du coup on effectuer la transition.

