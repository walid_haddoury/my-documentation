# React
## Installation
React est language creer par `Facebook`
Pour installer React :
- on cherhche sur internet `create react app` (on va sur le lien Github)
- on va sur le terminal et on tape `npm i create-react-app -g` ( l'installation est longue)
- on tape `create-react-app test` pour creer un nouveau projet ici qui sera appeller `test` 

## Information sur React
### JSX
En `JSX` on peux se permettre de renvoyer des balises
Exemple:  
![ExempleReturnADiv](assets/ReactReturnADiv.PNG)  
Avec JSX la syntaxe est tres particulière, on doit bien organiser nos balises (on ne peux mettre qu'une balise par ligne)

### JS native
En JS native les fonctions et les classes sont la meme chose, les `class` n'existe pas.
Du coup pour creer des class en JS native ont creer une fonction (la premiere lettre du nom de la fonction en majuscule pour distinguer la fonction de la 'class')
Mais du coup cette syntaxe de creer des class en JS native n'existe plus car c'est une version obselete de JS.
Exemple:  
![ExempleSyntaxeClassNative](assets/exempleClassJSNative.PNG)  

### Le fonctionnement d'un navigateur & DOM
#### Le principe d'un `Browser`
Le point d'entrer d'un navigateur c'est le HTML, et via le HTML on va y ajouter du CSS et du JS
On a toujours la page de base (principal) qui est le `index.html` 
Un browser fonctionne avec un principe de `TOKEN`  
![exempleSurLesToken](assets/ReactToken.png)  
le browser va interpreter le fichier HTML sous forme de TOKEN et via les TOKEN il va interpreter la page et les differente chose a effectuer.
le TOKEN 1 a pour propriete de contenir la balise H1
le TOKEN 2 a pour propriete de contenir le texte "titre"
le TOKEN 3 a pour propriete de contenir la fin de la balise H1

![marcheDuNavigateur](assets/ReactTheorieDeMiseEnMarche.png)  
En premier lieu le navigateur va lire la page HTML et tout ce qu'elle contient on fonctionant par TOKEN, et des que le Browser va tomber sur une balise `<script/>` elle va lire le JS ou le fichier ou est le JS en adaptant le JS sur les balise HTML en creer de nouveau TOKEN mis a jour avec le JS.

##### Rendering
Actualiser la page en fonction des modifications.
ETAPE DE LECTURE:
- Lecture de la V1 (version1)
- Interpretation du script
- Interpretation de la V2

Attention le JS peut etre modifier par des actions de l'utilisateur (exemple: clique, pop-up, formulaire, etc...)

##### le DOM
Lors de la premiere lecture du HTML on creer des instances pour tout les TOKEN que l'on va rencontrer, et l'ensemble de ces instance s'appel le `DOM`
ETAPE DU DOM:
- V1 du DOM
- Lecture du JS
- relecture du DOM ave les JS (phase de stabilisation)
- stabilisation V2 (comparaison avec la stabilisation V1, si les version sont indentique on peux lancer l'affichage)
- affichage (rendering) on inclus le CSS dans cette etape.

##### Dirty checking (Angular)
Avec les dirty checking il y a 3 boucles: 
- HTML avec creation du DOM
- lecture du JS
- lecture d'angular

Sauf qu'ici la Angular se met a jour a chaque nouvelle action (clique, mouvement de souris, etc...) la boucle Angular met a jour constament le DOM
C'est une technique qui est assez gourmande en ressource car elle la page s'actualise en continue.

##### Le virtual DOM
Le virtual DOM est une creation de React, qui est une façon de fonctionné differente du `Dirty Checking` d'angular.
Etape du virtual DOM:
- Lecteur du DOM 
- Lecture du JS
- Lecture du React et creation d'un virtual DOM ( copie du DOM) avec application du React sur le virtual DOM
- Re-stabilisation du Virtual DOM (si les deux version du Virtual DOM sont identique, application du virtual DOM sur le DOM)
Par contre contrairement a Angular, les boucles se stopent une fois le virtual DOM stabilisé et se relance seulement a une nouvelle action (clique, etc...)
Cette technoligie est moins gourmande en ressource qu'Angular.

 