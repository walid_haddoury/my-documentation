# Le Node, CRUD 

## Node
- Pour télécharger `Node` :
    - https://nodejs.org/en/ 
- Pour voir si `Node` est installer :
    - Aller dans le terminal et taper `node --version`
- Avec Node on peux telecharger des mini applications de dev' 
    - npm permet de lancer ces applications 
    - npm est un gestionnaire de paquet 
- pour faire du correctement on fait : 
    - On va dans WebStorm, `File`, `Settings`, dans la barre recherche on tape `node`, il y a alors `Node js ans NPM ` et on clique sur `Coding assistance for Node.js`
- Apres l'installation de `Node`, on peux lancer node depuis le terminal, on peux voir les console.log depuis le terminal
## npm
- Pour initialiser npm :
    - On regarder si il est pas deja installer, on regarde si il y a pas un fichier  `package.json`
- Dans le fichier `package.json` il y a deux type d'information :
    - Les `devDependencies`
    Aplications dont on va besoin pour dev' (application utile pour le dev')  
    Exemples : `sass-loader`, `css-loader`, `file-loader` permet de charger les feuilles de style ont les ressources en question.    
    `babel-loader`, permet de traduire le JS en js international
    - les `dependencies`
    Applications qui vont etre utiliser par le produit final, donc le client
- Pour installer et ajouter une application dans `package.json` :
    - On fait `npm i -S (nom de l'app` (pour ajouter l'application dans les `dependencies`), et `npm i -D (nom de l'app)` (pour ajouter dans les `devDependencies`)  
Le client n'aura pas acces aux `devDependencies`, mais seulement aux `dependencies` car il en a besoin pour le fonctionnement de l'application.
## Le CRUD
- CRUD : Creat Read Update Delete
- Un CRUD est l'ensemble de ces opérations, donc lorsque l'on effectue un CRUD celui-ci doit etre capable de `read`, `add`, `delete`, `create`.


- CTRL + C raccourcis pour eteindre le serveur
- node + (nom du fichier) pour lancer le serveur local 


````javascript
const express = require('express');
````
veux dire : va chercher le logiciel express  

````javascript
const app = express();
````
veux dire : creer une application qui utlise express

````javascript
app.get('/', function(req, res) {
  res.send('hello world');
});
````
veux dire : si quelqu'un qui vient `get` le `localhost` a la racine tu revoie `hello world`

````javascript
const app = express();

app.listen(3000, function() {
    console.log('exemple app listen on port 3000')
})
````
veux dire : va aller ecouter sur l'un des ports `localhost` et va effectuer les operations

````javascript
app.get('/number', function(req, res) {
  res.send('8')
});
````
veux dire : si on fait `localhostel:3000/number` on aura une string `'8'` mais on peux pas renvoyer un nombre directement

````javascript
app.get('/hostels/:id', function(req, res) {
  const id = parseInt(req.params.id);
  res.send(getHostelById(id));
});
````
veux dire : `res` est le parametre qui permet de repondre a l'utlisateur et `req` celui qui permet de recuperer la requete, `req.params.id` va recuperer le parametre que l'on a mis `'/hostels/:id`, le `parsInt` permet de changer une string en `int`  

## Postman

- Telecharger `Postman` : https://www.getpostman.com/downloads/

- Une fois telecharger on peux l'utiliser sans creer de compte(aller en bas de la fenetre)

`get` pour afficher
`delete` pour suprimer
`post` pour ajouter 
`put` pour remplacer
