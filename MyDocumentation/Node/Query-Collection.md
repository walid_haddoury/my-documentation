# Query Collection

Permet de recuperer des Data.
On peux aller voir comment faire sur la doc de firebase --> Cloud FireStore --> Get data once --> Get all document in a colletion


Pour pouvoir recuperer les data on doit faire une fonction pour prendre notre data:  
![exempleFonctionDataRecup](assets/queryCollectionExemple1.PNG)  
- Ne pas oublier les promesse avec Async Await
- On creer une fonction qui va avoir pour but de de recuperer la data et de la stocker dans une Arrays comme on l'a a l'original.
- On creer une `const list` dans laquelle on lui dit quelle collection il doit aller, et on lui de faire `get` dessus (pour les prendres)
- Suite a ça on creer une `const hostels` d'un tableau vide.
- On parcours ensuite `list` grace a un `forEach` et on `push` tout ça dans `hostels` et ont fait bien `.data` pour ne recuperer que les data
- Pour le typage :
    - `list` est de type `QuerySnapshot` c'est le type que renvoie `.get`
    - `hotels` va etre de type `HotelModel []` (car on aura un tableau avec plusieurs HotelModel)
    - `hotel.data` va etre de type `HotelModel` car on recupere seulement la data qui sont les hostel qui sont de type `HotelModel` car on les recupere un par un.

![exempleUtiliserLaDataBase](assets/QueryCollectionExemple2.PNG)  
- On recupere notre DataBase dans hostels
- On creer une copie de notre DataBase dans hotelsClass avec les HotelClass pour pouvoir utiliser les class dessus.
