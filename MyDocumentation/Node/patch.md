# `app.patch`  
## Que fait patch ? 
- `patch` permet de modifier un element precis dans une base de donnée mais le `patch` ne peux pas ajouter de donnée.
la fonction `patch` va parcourir les `object` ou les `arrays` pour comparer avec les nouvelles informations mise dans le `body`, de `postman`, et ci les `keys` des données correspondent aux données que l'on a, on pourra alors effectuer la modification de la `Value`.

## Syntaxe d'un `patch`
- EXEMPLE :
````javascript
app.patch('/hostels/:id', function(req, res) {
try { // permet de de faire nos focntions mais des que l'on va trouver un throw ça va renvoyer une erreur sans faire crash tout le serveur
const dataToChange = req.body; // pour recuperer le body que j'ai creer dans postman
const id = parseInt(req.params.id); // recuperer le :id dans le lien et le transformer en number
const index = hostels.findIndex(value => value.id === id); // .findIndex pour avoir l'index de l'hostels a modifier
let hostelToChange = hostels[index]; 
const keysToChange = Object.keys(dataToChange); // Keys de mon body dans postman
const authorizedKeys = Object.keys(hostelToChange); //Keys d'hostels

const isAuthorized = keysToChange.map(key => authorizedKeys.includes(key)); // .include permet de voir si l'object est dans le tableau et renvoie true ou false

const isOk = isAuthorized.every(key => key); // every va regarder si il que des true, si c'est le cas il renvoie true, sinon false
if (!isOk){ // si isOK n'est pas true 
throw new Error('non authorized key'); // permet de renvoyer une erreur si la fonction n'est pas respecter  
}
Object.assign(hostelToChange, dataToChange); // fait le meme travail que le spread operator en dessous, mais on a pas besoin par la suite de faire un put pour envoyer nos information car le assign fait le changement a la racine (marche avec des pointeurs)
hostelToChange = {...hostelToChange, ...dataToChange}; // va permettre de mixer les informations de hostelsToChange avec dataToChange donc de modifier ce que l'on veux
putHostel(id, hostelToChange); // On appel notre fonction qui modifie un hostels pour appliquer les changements sur hostels 
res.send(hostelToChange);
} catch (e) { // une fois l'erreur recuperer on renvoie une erreur sans faire crash tout le serveur ou les autres fonctions 
 console.log(e.message);
res.status(400).send(e.message); 
}
})
````

## La gestion d'erreurs 
- Pour gerer la gestion d'erreur on utlise `throw`, qui va permettre de renvoyer une erreur 
exemple : 
````javascript
throw new Error('non authorized key'); // renvoie une erreur avec le texte qu'on a mis et renvoie le code 500 pour internal error 
````

- Pour controler, les bugs ou crash de serveur on utlise `try` et `catch` et evité que d'autre fonctionnalité du site ou application ne crash
````javascript
try {
 // nos function
if (ok){
throw new error('erreur de merde'); 
}
}catch (e){
res.status(400).send(e); // renvoie le statut 400 et renvoie le message d'erreur du throw
}
```` 
