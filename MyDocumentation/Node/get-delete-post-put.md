- Pour pouvoir utliser `experess`pour le CRUD :
````javascript
const express = require('express');
const app = express();
````

- Pour pouvoir utliser le `post` et le `put` :
````javascript
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
````

- EXEMPLE `get` :
````javascript
app.get('/hostels/:id', function (req, res) {
    const id = parseInt(req.params.id);
    const theHostel = returnHostel(id);
    res.send(theHostel);
});
````

-EXEMPLE `delete` :
````javascript
app.delete('/hostels/:id', function (req, res) {
    const idHotel = parseInt(req.params.id);
    res.send(removeHostel(idHotel));
});
````

- EXEMPLE `post` :
````javascript
app.post('/hostels', function (req, res) {
    const newHostel = req.body;
    const addedHostel = addHostel(newHostel);
    res.send(addedHostel);
});
````

- EXEMPLE `put` :
````javascript
app.put('/hostels/:idHostel', function (req, res) {
    const newHostel = req.body;
    const indexHostel = parseInt(req.params.idHostel);
    const changedHostel = putHostel(indexHostel, newHostel);
    res.send(changedHostel);
});
````