# Git Flow

## Les `branch`
### Qu'est ce qu'une `branch` ?

Une `branch` est un espace qui permet de travailler a plusieurs sur un projet et en pushant sans gener les autre ni effacer leur travail.
- On peux autant de `branch` que l'on veux, et il faut biensur les nommé en fonction de ce que l'on va faire.
- Les changement effectuer sur le fichier source ou bien l'ajout de nouveau fichier ou dossier ne va pas etre mis a jour pour les autres personnes.
il faut que la personne en charge de la `branch Master` valide le travail et accepte les ajouts, via une `merge request`.
- Pour un soucis de norme lorsque l'on creer une `branch` on l'a nomme `feature/(le nom qu'on veux)`.  

- La branche `Master` est la branche principale, celle qui va etre `push` sur le `CDN`.

## Comment creer une `branch` ?
- VCS 
    - Git
        - Branches
            - New Branch  
            ![New branch](New-Branch.PNG)  
            Donner un noms qui commence par `feature/(tache a faire)`

## `Merge`

### `Merge` soit même

- Pour merge soit meme ont va :
    - VCS
        - Merge changes
            Il y a les merge lié au disque dur du pc `feature/(etc...)`
            Et les les merge du serveur git `remote/origin/feature/(etc...)`  
            ![merge-type](type-de-merge.PNG)  
            une fois le merge fait on ne peux pas commit car de changement détecter mais on peux push les commit fait par l'autre `branch`.
   
### Supprimer une `Branch`

Pour supprimer une `branch` on peux supprimer la `branch` localement ou bien à distance (celle du git)  
![delete-branch](Delete-branch.PNG) 

Quand 2 personne qui ont sur leur `branch` une modification de la même chose il va y avoir un conflit et il va falloir choisir entre les proposition et choisir celle que l'on veux.  

### Comment demander une `Merge Request` ?

- Pour faire une demande de `merge request` on va sur `GitLab`, `create merge request` (on peux changer la destination du `merge request`)
- le reponsable de la branch Master va recevoir un mail et une notification sur `GitLab`, il aura accès aux different `commit` et modification apporter au code.  
- Une fois la `merge request` accepter, ont pull la `branch Master`.  

## Infos divers

- Essayer de commit un maximum pour la moindre modification ou ajout. Bien expliquer le motif du commit pour une meilleur lisibilistée.
- Quand on passe d'une branch a l'autre on dit qu'on fait un checkout  