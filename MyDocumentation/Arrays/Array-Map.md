# Array Map

Un Array Map permet de creer un tableau dans lequel on va pouvoir choisir, l'index et la value du tableau.

Pour creer un Array-Map:
````javascript
app.get('', async function (req, res) {
    try {
        const ownerMap = new Map();
        const owner: OwnerModels[] = [];
        const ownerSnap = await db.collection('owners').get();
        ownerSnap.forEach(ownersSnap => owner.push(ownersSnap.data() as OwnerModels));
        owner.forEach(owners => ownerMap.set(
            owner && owners.lastName ? owners.lastName.charAt(0).toUpperCase() : '',
            owner && owners.lastName ? owners.lastName.charAt(0).toUpperCase() : '')
        );
        console.log(ownerMap);
        return res.send(Array.from(ownerMap.values()).sort((a, b) => a < b ? -1 : 1));
    } catch (e) {
        return res.status(500).send(e);
    }
});
````
Le `ownerMap.set()` permet de choisir notre `id` qui sera lié a notre `value`