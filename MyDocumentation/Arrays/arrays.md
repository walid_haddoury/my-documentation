# Les Arrays
## Qu'est ce qu'une `Array` ?
- Une arrays est un tableau en anglais, dans lequel on peux mettre divers chose différentes comme des `nombres`, des `strings`, ou bien meme des objets 
- On déclare une array vide:
 ````javascript
const tab = [];
````  
- Dans un tableau il faut avoir toujours le meme type d'information a l'interieur, ne pas mélanger objet avec nombre et string.  
Car on peux sinon creer des conflits.
- Une array avec des nombres, des strings, et des objets
````javascript
const tab = [1, 655, 89863, 'bonsoir', {firstName: 'seb' age: 156}]
````
## Outlis pour les arrays

- `length` pour avoir la taille du tableau afficher dans console.log
````javascript
console.log(tab.length);
````
- Pour avoir une case pécise du tableau, ici pour la case 2.
````javascript
console.log(tab[2]);
````
- Pour avoir le dernier element du tableau, si on ne fait le -1 on sera une case apres le tableau.
````javascript
console.log(tab[tab.length - 1]);
````
- On peux addition 2 case du tableau mais additionner seulement les case du meme type(2 cases ou il y a un nb, si on addtionne 2 chose differente ou une case hors du tableau on au `NaN`)
````javascript
console.log(tab[2] + tab[3]);
````
## Les opérateurs

- `forEach`permet de chercher un element du tableau ou d'un objet et d'appliquer dessus une fonction.
````javascript
tab.forEach(element => console.log(element.age));
````
  
- `function` Les fonctions effectue une operation et renvoie le résultat
````javascript
tab.forEach(element => byTwo(element.age));

function byTwo(x) {
  return x * 2
};
````

- `map` permet de chercher un element et de le transformer 
````javascript
const tableauDesAges = tab.map(element => element.age)
````

- `filter` permet de filtrer les information avec un prédicat, ne seront dans `moinsDe40` que les personne de moins de 40, les autres ne seront pas afficher.
````javascript

const moinsDe40 = tab.filter(element => element.age < 40)
console.log(moinsDe40);
````

- `sort` le sort de ranger par ordre numérique, alphabétique, etc...
````javascript
const orderedTab = tab.sort( compareFn: (a, b) => a.age - b.age)
console.log(orderedTab);
````  
Pour ranger en ordre alphabétique :
````javascript
const orderedTab = tab.sort( compareFn: (a, b) {
    if (a.firstName < b.firstName){
        return -1;
    }
    if (a.firstName > b.firstName){
        return 1;
    }
})
console.log(orderedTab);
````

- `every` permet de vérifier si tout le tableau remplis la contion et nous renvoie un Booléen `TRUE` ou `FALSE`
 ````javascript
const areAllMajor = tab.every(element => element.age > 18)
console.log(areAllMajor);
````

- `some` permet de vérifier si un élement du tableau vérifie la condition et renvoie `TRUE` ou `FALSE`
````javascript
const areAllMajor = tab.some(element => element.age > 18)
console.log(areAllMajor);
````

- `push` pour ajouter une element a un tableau
````javascript
const newPersonn = {firstName: 'Seb', age: 45}; 
tab.push(newPersonn);
console.log(tab);
````



- infos divers
    - **super important** respecter la norme, éviter les espaces, les sauts de ligne initile.  
    Racourcis pour la norme : CTRL + ALT + L