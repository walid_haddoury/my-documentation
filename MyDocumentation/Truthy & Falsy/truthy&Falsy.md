# TRUTHY & FALSY
## Les `falsy`

- Un `falsy` est une valeur equivalent a `false`.
- Les types de `falsy`:
    - `false`
    - `0`
    - ` '' ` (une string vide)
    - `null`
    - `undefined`
    - `NaN`
Le `falsy` permet surtout de verifier si un element existe.

## Les `truthy`

- Un `truthy` est un element qui existe
- Les `truthy` sont tous les inverse de `falsy`
un nb positif ou négatif est un `truthy`, 
