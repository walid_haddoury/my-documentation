# Boostrap et CSS

## Combiné Boostrap et CSS

### Pourquoi combiné Boostrap et CSS ? 

- Bootstrap permet de trouver des fonctionalitées deja préfaite mais le soucis, est lorsqu'on veux modifier ces préfabriquer. Comme la couleur, la taille, le positionement, etc...

- Il y a des chose que l'on veux faire qui n'existe pas.
    - Exemple :  
    Une demande extravagante unique qui ne correspond a rien dans Bootstrap. Comme un texte qui clignote, a un certain rythme.
     

### Modifier Bootstrap ou mixer Boostrap et CSS ?

- Modifier Bootstrap :
    - Modifier Bootstrap est possible et parfois pratique mais compliquer en fonction du but.
    
- Combiner Boostrap et CSS :
    - Pour ce faire on doit creer une page CSS qui contiendra les modification que l'on voudra sur l'élément en question.
    
    Pour bien organiser ces differentes page CSS il est conseiller de toute les ranger dans un dossier et de faire limite un fichier CSS pour chaque modification (meilleur pour les modifications si besoin).