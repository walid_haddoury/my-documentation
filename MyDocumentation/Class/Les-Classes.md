# Class
## Principe des class
Les class permette de  d'organiser par type
## Raccourcis pour le constructor
Alors une fois qu'on a copier les proprietés de de notre model on clique droit sur le nom de notre class pusi on choisis `generate` et on choisis ce que l'on veux dans notre `constructor` 
Il sera generer automatiquement
## Comment creer une class

Pour creer une class vaux mieux etre dans un dossier ou TypeScript est installer, car fonctionnalité des class sont avec TypeScript.
````typescript
export class HotelClass { // pour exporter la class et quand donne un nom a une class on commence avec une majuscule et on fini avec class
    id?: number; // dans une class on doit avoir des proprietés
    name?: string;
    roomNumbers?: number;
    pool?: boolean;
    
    constructor() {}
}

const myHostel = new HotelClass(); // veux dire que l'on va creer une class avec les proprieté de la class HotelClass (avec id, name, roomNumber etc..)
 
````

### `class` et `constructor`
Exemple 1:  

![exemple1](assets/ClassExemple1.PNG)  
Alors dans cet exemple on a creer une class qui a les memes proprieté que HotelModel,  
dans le constructor on dit de faire console.log('created').
`const myHostel = new HostelClass();` permet de creer une une instance de la class HotelClass ce qui veux dire qui a les proprieté de la class HotelClass.

### `this`
Exemple 2:
  
![exemple2-1](assets/ClassExemple2-1.PNG)  
![exemple2-2](assets/ClassExemple2-2.PNG)  
Alors ici on a mis dans le constructor id de type number car on attend de lui que ce soit un number
Puis grace a this.id on va attribuer l'id a this.id qui est la future instance donc le future produit finale
Puis pour cela dans notre .get ont donne la valeur 12 qui va etre transmise au constructor de HotelClass  
![resultExmple](assets/ClassExemple2-3.PNG)  
Du coup on a une instance avec seulement la proprieté id = 12
  
### `?`
Exemple 3:  

![Exemple3](assets/ClassExemple3.PNG)  
Ici supposont que l'on veux entrer certaine informations comme pool, roomNumbers, et isValidated bah alors dans le constructor on ajoute un `?` pour dire que l'information est optionnel donc pas obligé a entrer.

### `Object.assign`
Exemple 4:
![Exemple4-1](assets/ClassExemple4.PNG)  
Ici on a pas besoin dans le constructor de declarer tout les type que l'on va avoir, on fait donc a la place un `Object.assign` et on va attribuer `hotel` a `this` et on va dire au constructor que hotel sera de type `HotelModel` car on a deja typé HotelModel.  
![Exemple4-2](assets/ClassExemple4-2.PNG)  
Par contre quand on va appeller notre class on va devoir dire que c'est un `Object` car le `constructor` attend un `HotelModel` qui est un `Object`  

### Creer des fonctions dans les class
Exemple 5:
![Exemple5-1](assets/ClassExemple5-1.png)  
Ici on a creer la fonction `calculateRoomNumbers` qui permet de donné automatiquement le RoomNumbers, pour cela il ajouter `rooms` dans la class et vue que rooms de base il avait un `?` il pouvait etre undifened du coup il faut l'enlever et creer un `this.rooms = hotel.rooms;`  
![Exemple5-2](assets/ClassExemple5-2.PNG)  
On appel notre fonction sur myHotel et ensuite on fait notre `res.send` et le roomNumber sera ajouter et mis a jour  

### Enchainer les appels de fonctions
Exemple 6:  
![exemple6-1](assets/ClassExemple6-1.PNG)  
Pour creer des fonctions qui peuvent s'enchainer on doit return a la fin de la fonction `this`  
![exemple6-2](assets/ClassExemple6-2.PNG)  
Du coup maintenant on enchainer les fonctions dans `app.get`  

### `extends` `super()` `||`
Exemple 7:
![exemple7-1](assets/ClassExemple7-1.PNG)
Pour creer un hotel avec les memes proprietes que `HotelClass` on creer une nouvelle class et on extends la class HotelClass dans la class dans la nouvelle class,
on met le constructor dans la nouvelle et on ajoute `super(hotel)` pour prendre le constructor le la class HotelClass et ajouter de nouvelle chose au constructor 
On doit aussi creer un model dans lequel on va extends le HotelModel et on ajoute a l'interieur les nouvelles proprieté que l'on veux.
le `||` permet de donner une valeur par default au cas ou on aurait oublier de la declarer.  

### `implements`
Exemple 7:
![exemple7](assets/ClassExemple7.PNG)
Ici on creer une juste une class pour les rooms mais ici on ajoute `implements` qui permet dire que dans la class RoomClass il y aura exactement les meme proprieté que dans roomModelet que si ont veux ajouter une proprieté on doit aussi l'ajouter a roomModel

### Set des valeurs par delaut dans le constructor
Exemple 8:
![exemple8](assets/ClassExemple8.PNG)
On peux donner des valeurs par default avec les egales