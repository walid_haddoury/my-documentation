# Await Async

## Le Await Async
Lorsque l'on veux faire un `await async` il faut toujours mettre au debut un `try` `catch` 
Dans les promesses le `catch` existe deja on a pas besoin de le creer, alors que dans `await` `async` il faut qu'on creer le `catch` pour revoyer les potentiels erreurs

````javascript
app.get('/', async function(req, res) { // le async permet de dire que l'on va travailler en asynchrone
  try{
        const hotelSnapshot = await db // le await permet de dire que tant qu'on a pas le resultat de la const on ne fait rien d'autre
            .collection('hostels')
            .doc('(key of the doc)') // key d'un hostel dans la database
            .get();
        const hotel: HotelModel = hotelSnapshot.data() as HotelModel;
        hotel.isValidated = true;
        res.send(hotel);
    }catch (e) {
      res.status(500).send(e);
    }
})
````

## Utiliser Await Async avec Promise.all
 
````javascript
app.get('/', async function(req, res) { // le async permet de dire que l'on va travailler en asynchrone
  try{
        const hotelPromise = db // permet de preparer la promesse sans la jouer 
            .collection('hostels')
            .doc('(key of the doc)') // key d'un hostel dans la database
            .get();

        const hotelPromise2 = db 
            .collection('hostels')
            .doc('(key of the doc)') // key d'un hostel dans la database
            .get();
        // ici dans la const on fait une destructuration d'un tableau c'est comme si on faisait [1, 2]
        const [hotelSnapshot, hotelSnapshot2] = await Promise.all([ // ici on doit mettre await devant Promise.all pour jouer toutes le promesse en meme temps, et pour cela on doit stocker le resultat de toutes ces promesse dans une const
                hotelPromise, 
                hotelPromise2]);
        const hotel1 = hotelSnapshot.data();
        const hotel2 = hotelSnapshot2.data();
        res.send([hotel1, hotel2]);

        
    }catch (e) {
      res.status(500).send(e);
    }
})
````

## `as`
Le `as` permet de definir un type sur quelque chose, on peux forcer le type grace a ça.
Le `as` est pratique quand on sur du type de donné que l'on va recevoir, cela permet aussi de forcer un typage dans certain mais oblige a reflichir a ce que l'on fait car `TypeScript` est tres strict.
Exemple:
`hotel.data() as HotelModel` 
Ici on dit que `hotel.data()` aura forcement `HotelModel` comme type.
