# Herbergement

## Les types d'Assets

### Assets Statiques
- Les Assets statiques sont tout les fichiers ou code qui ne seront plus modifié comme :
    - HTML
    - CSS
    - JS
    
### Assets Dynamiques
- Les Assets dynamiques sont toutes les données qui peuvent varié et être modifier :
    - Base de données
    - Data

Les données ne sont pas fixe elle peuvent changer, comme le noms d'utilisateur, nouvelle notifications, etc...  
Ces données qui sont variable vont etre envoyer aux fichiers statiques mais le fichier statique ne changera pas (le code ne va changer)

## Le CDN

### Qu'est ce qu'un CDN ?

Un CDN (Content Delevery Content) est un principe qui permet de copier le contenu d'une page internet vers d'autre ordinateur dédier a cela a travers le monde pour que quand un utilisateur veux accéder au site il puisse le trouver grace au PC le plus proche de chez lui.
C'est donc u réseau de mise a disposition de contenu 

### Installation d'un CDN

Le CDN est question est FireBase : 
- Aller sur https://firebase.google.com/

- Créer son compte

- Creer un projet 
    - Aller sur Hosting
        - Copier le lien et le mettre dans le terminal de WebStorm (pour installer Firebase sur WebStorm)
        - Ensuite utliser les commandes `firebase login` et `firebase init` (ou `firebase init hosting` pour init seulement pour le hosting)
        - Selectionner le projet que l'on veux Host
            - Creer un dossier "public" ou l'on va mettre tout les dossiers et fichier de nos pages, puis mettre a la racine du dossier public le fichier `index.html` et autre fichier de la première page.
        - Une fois `init` terminer, faire `firebase deploy`, on aura ensuite le lien de notre page internet.

## Git ignore firebase
-  Ajouter dans le fichier `.gitignore` le nom `.firebase` 
- Ne pas ajouter les autres dans `gitignore` car on en a besoin  