# Installer la Data-Base de Firebase

## Aller dans le fichier `environement.ts` d'angular et ajouter:  
![exmepleEnvironement](assets/environement.png)
description : le lien HTTP est celui de la `Cloud functions` que l'on a creer dans firebase

## Creer un service sur angular:  
- faire `ng g s (nom du service)` [`ng` pour angular, `g` pour generate, `s` pour service]
- dans le fichier service ajouter `HTTP client` dans le constructor pour pouvoir faire des appels
- Creer des fonctions qui vont utiliser nos cloud functions creer dans le Back Office
![exmepleService](assets/services.png)
description : on return l'instance de `this`, de `http` sur laquelle on fait un `post`, on appel `environement.root` qui est notre lien qui envoie vers notre Datebase et ajoute la fin du lien de la fonction que l'on veux utiliser, et enfin l'objet dont on a besoin pour faire fonctionner notre `Cloud Functions`.  

## Les modules
- Dans les modules ont a tout les fichiers dont a besoin pour faire fonctionner notre module, via des `imports`  
![exempleImportModule](assets/importModule.png)  

## Les components  
- Dans les components ont fait nos functions, on a un `constructor`, et sous le constructor on peux ecrire nos functions  
![exempleComponent](assets/functionComponents.PNG)  

## Récuperer les datas d'un formulaire pour les envoyer au Back End
- On creer un formulaire dans le component avec toutes les information que l'on aura, ensuite creer le formulaire sur le HTML du module.
![exempleForm](assets/FormJSON.PNG)  
Le `button enregistrer` permet de recuperer les informations que l'on a dans la page et de les envoyer dans ma const `ownerForm`
