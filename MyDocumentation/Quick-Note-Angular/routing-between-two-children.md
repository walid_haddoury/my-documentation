# Routing entre deux Module Enfant

- Creer nos modules avec nos `route`
- Ne pas déclarer le modules dans le `app.module.ts`
- Ne pas déclarer la route dans le `app.routing.module.ts`
- Ni dans le `routerLink`

## Aller naviguer entre deux modules
- Creer une route dans ce module dans le `("Name").routing.module.ts`
![routingPrincipal](assets/routingPrincpal.png)
On est dans le `directoryOfOnersByLetter.module.ts` et on une fois que l'on a cliquer sur un bouton, on est renvoyer sur le module 
`filteredOwners.module.ts`

- Pour creer notre `:id` pour appeller notre `filteredOwners.module.ts`
![routerLink](assets/routerLink.png)
On creer notre `routerLink` dans lequel on met `i` qui va etre notre `:id` que l'on va utiliser dans le module


## Reécuperer une `:id` dans une URL
- Creer une `const letter ou id` dans laquelle on va stocker notre `:id` que l'on aura recupere dans l'URL
![getIdFormURL](assets/idFormURL.png)
Pour avoir on a besoin de `ActivatedRoute` que l'on va initialiser pour pouvoir `get` l'URL
![importPourGetURL](assets/importToGetIdFormURL.png)
