# Installation d'angular

Etape d'installation d'angular:
- Aller sur le depot Angular
- Entrer dans le terminal et taper `npm install -g @angular/cli`
- Ensuite faire `ng new (nom de l'app)`

Pour executer le code Angular: faire `show npm script`, puis lancer `start`

Pour creer des components:
- Dans le terminal faire `ng g m (nom du module)`
- Ensuite faire `ng g c (nom du module)` le component aura le meme nom que notre module


## Creation de Route
### Mise en application des `Routes`
- Il n'y a qu'un `forRoot` qui est la route parent, pour ajouter d'autre root il faut creer un fichier `routing` dans chaque `module`  
![exempleRouting](assets/routing.png)  
- Pour creer une route : aller dans le component et creer un fichier qui s'appel `(nom du fichier).routing.module.ts`, creer un `path` et ajouter le component lié, et ne pas oublié de changer le `forRoot` en `forChild`    
![exempleRoutingChild](assets/forChild.png)  

### Utilisation des routes
- Aller dans le fichier `app.component.html` et ajouter la balise `<routing-outlet>`
- Pour creer des boutons qui renvoie vers un `module` on utilise `[router-link]` puis ajouter le path que l'on a defini dans `app.routing.module.ts`
![exempleRouterLink](assets/router-link.PNG)