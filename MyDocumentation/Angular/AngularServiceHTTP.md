# Angular Service HTTP

Avec `Angular` pour recuperer des données ont ne le fait pas dans un component (INTERDIT), on utilise pour le faire un outils particulier `Le service`, c'est une class qui a un decorateur spécial qui lui donne des super pouvoir( qui est le `singleton`)

`Singleton` : c'est une class qui n'a qu'une seul instance (On ne pas creer une instance differente, si on essaye de refaire un `new` on se retrouve avec l'instance de base)

Pour creer un `service` : 
- On regarde que l'on se trouve bien dans le dossier `myAngular`
- On tape la commande `ng g s hostels`  (ng = angular, g = generate, s = service, hostels = le nom du service que l'on veux lui donner)
Les fichier vont se creer automatiquement dans le dossier `app` :
- il y en a un qui est `spec.ts` qui sertr pour les test unitaire (on l'enleve pendant la formation)
- hotels.service.ts : 
    - A l'interieur du fichier on a : 
        - Une `class`, `HostelsService`
        - un decorateur  
         ![exmeple1](assets/ExempleAngularService.PNG)    
         permet de dire que le singleton est lié a l'application (root de l'application)
         

Dans la class `HostelsService` on va faire nos appel au back office, pour faire appel au back office on va devoir faire des appel HTTP, en Angular quand on a besoin d'un outils et qu'on veux s'en servir on le declare dans le `constructor`

Dans Angular quand on veux utiliser un outils il faut le declarer dans le fichier `app.module.ts`  
![exempleOutils](assets/exempleServiceHTTP.PNG)    
le `BrowserModule` permet de faire l'affichage de la page  
le `AppRoutingModule` permet de faire les `route`  
le `FormsModule` permet de faire fonctionner la balise `input` avec `ngModel`  

Dans le `import` on tape `HttpClientModule` pour ajouter le module qui permet de faire des appels au back-end.    
![exempleService2](assets/exempleAngularService2.PNG)    
le mot clé `private` permet de dire que l'on va utiliser l'outil seulement dans le `constructor`
le `HttpClient` c'ets lui qui va permettre de faire les appels
`httpClient` au debut on peux mettre ce que l'on veux, mais ensuite on peux faire `Shift + F6` et il va nous proposer directement un noms.


Pour recuperer nos data on va devoir faire une fonction a l'interieur de notre class `HostelsService`
![exempleHTTPclient](assets/angularServiceExemple3.PNG)  
Pour pouvoir utiliser le `HttpClient` qui est dans le `constructor`, on doit appeller `this` puis `httpClient`, et comme ça on peux utiliser toutes les fonctionnalités de cette class  
Il y a plusieurs verbe que l'on connait deja (get, patch, put, post, delete)

### Observable 
Un `observable` est a peu pret la meme chose qu'une promesse mais en plus puissant.



### Le Cross Origin
Le `Cross Origin` est le fait de prendre le lien des data d'un site pour un autre site
Chaque site doit payer sa bande passante pour utiliser ses data (coute tres chere), mais certain prenne le lien d'autre site pour utiliser la bande passante du site sans autorisation.

`Activer le Cross Origin` est que le proprietaire d'un site accepte de donner sont lien a un autre site avec un contrat.

Etape d'activation du Cross Origin: (A faire dans le dossier ou il y anotre data base (ici tsSrc))
- Aller sur `nmp cors` (site internet)
- Aller sur le dossier `fonctions` de `tsSrc` dans le terminal 
    - Taper la commande `npm i -S cors` (Pour enregistrer l'app dans les `dependencies` du `package.json`)
- On va dans `index.ts` dans `tsSrc` --> `src`
    - On ajoute la ligne `const cors = require('cors');`
    - On ajoute aussi `app.use(cors());`
    - Finir par relancer `watch` et `serve`

Utiliser le Cross Origin: (faire ça dans le dossier front)
![exempleCrossOrigin](assets/AngularServiceExemple.PNG)  
Ajouter tout ça dans le fichier `hostels.service.ts` dans la class
On fait un `.get` sur le `httpClient`, on met le lien de notre data base que l'on a sur `tsSrc` 
On peux typé le type de data que l'on va recevoir `.get<HotelModel[]>` nos data seront de type HotelModel[]
`.pipe()` est un tuyau dans lequel les data vont circuler, on va pouvoir interagir avec les data comme ça
`.tap()` permet de faire une action dessus, on l'action est de faire un console sur les data.

Aller dans le fichier `app.conponent.ts`
![ExempleAngularService](assets/ExempleAngularServiceCrossOrigin.PNG)  
On creer un constructor dans lequel on va appeller `HotelsService`
On creer la fonction `getHostels` qui va appeler `hostelService` et la fonction `getHostels` et dessus on fait un `.subscribe()` pour s'abonner au data.
Ensuite pour que la fonction s'effectue des le lancement de la page, on doit ajouter `ngOnInit()` 
Pour ajouter `ngOnInit` on doit ajouter avant une class, on fait alors `implement OnInit`, ensuit on peux appeller `ngOnInit` dans lequel on va appeller la fonction `getHostels()`


### Pour pouvoir afficher notre data base: 
![exempleAfficherLaDataBase](assets/exempleAngularServiceCrossOrigin2.PNG)  
Dans le fichier `hostelsService.ts`  
On enleve notre `.pipe` car on en a plus besoin.
On type notre fonction, c'est un `Observable` de type `HotelModel` et vu que `getHostels` est un Observable on ajoute a la fin un `$` pour spécifier que c'est un Observable 

![exempleAfficherLaDataBase2](assets/exempleAngularServiceCrossOrigin3.PNG)  
Dans le fichier `app.component.ts`
On SHIFT + F6 le `$` va se mettre automatiquement la il le faut
On creer un `hostels: HotelModel[];`, on va lui attribuer `getHostels$` dans le `.tap`
Dans le deuxieme `.tap` on va juste faire un console.log pour regarder si `getHostels` recupere bien les data.
Grace a ça on peux reutiliser notre `hostels` dans `app.component.html` 

![exempleAfficherLaDataBase1](assets/exempleAngularServiceData1.PNG)  
Maintenant que l'on a `hostels` qui a une valeur on peux essayer de l'afficher, grace a un `{{hostels}}`, on aura un en affichage `[object Object]`
Du coup on ajoute `|` qui veux dire, convertit en, ici en json

![exmepleAfficherLaDataBase2](assets/exempleAngularServiceData2.PNG)  
On veux afficher les hotels un par un, avec `<ul>` on creer une liste
Dans `<li>` on veux utiliser une boucle `for` pour parcourir tout les hotels, on a un outils qui est similaire a la boucle `for` c'est `*ngFor`
On creer un `let hostel of hostels` qui veux dire que hostel sera l'hotels qu'il parcour dans hostels, `i` permet d'avoir l'index 
Ensuite quand on fait l'appel on peux choisir les proprieter que l'on veux afficher ou pas.
 