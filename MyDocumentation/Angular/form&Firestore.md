# Form & Firestore
Utiliser Firebase dans notre application `angular`:
- Aller sur le site de Firebase
- Aller dans notre projet
    - Cliquer sur `Ajouter une application`  
    ![exempleEtape1](assets/exempleForm1.png)  
    Aller sur application WEB  
    ![exempleEtape2](assets/ExempleForm2.png)  
    - On donne un noms a l'application  
- On retourne dans WebStorm dans notre dossier `myAngular`
    - src --> environement --> environement.ts
    - Ajouter :  
    ![exempleEtape3](assets/exempleForm3.png)  
    copier la partie rouge et l'ajouter dans le fichier `environment.ts`    
    ![exempleEtape4](assets/exempleForm4.PNG)  
    - Ensuite aller dans `environement.prod.ts`  
    ![exempleEtape5](assets/exempleForm5.PNG)  
    