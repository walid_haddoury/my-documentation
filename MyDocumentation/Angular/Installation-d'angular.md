# Installation d'angular
## Etape d'installation 

- Creer un dossier `front` a la racine de `tryHard`
   - Faire l'installation d'angular a l'interieur

- Aller sur https://cli.angular.io/
    - Copier `npm install -g @angular/cli`
        - Faire `y` pour changer les settings

- Pour regarder si angular est installer et connaitre sa version: `ng --version`

- Pour creer un dossier Angular:
    - `ng new (nom du dossier)`
        - Mettre `y` pour Angular routing
            - Choisir `SCSS`
            
Pour lancer le serveur avec Angular:
    - Ouvrir `Package.json`
        - show npm script et lancer `start`

## Info divers

CLI : `Commande` `Line` `Interface`
np = Angular
`e2e` est pour les tests unitaire
Angular est en TypeScript
Angular creer ses propre balise HTML (ex: <app-root>), on peux aussi creer ces propres balises
La mode du moment est de mettre le `CSS` dans le meme fichier que le `JS` (mode stupide selon Seb, car il y a deja un fichier dedié au `SCSS` dans lequel on peux mettre le CSS), il n'y a pas de difference de performance que csoit dans le fichier du JS ou pas.
Un `component` est composer d'un fichier `HTML` d'un fichier `SCSS`, et d'un fichier `module.ts`
Les `{{ }}` = veux dire que l'on veux recuperer une propriete qui est dans le `Component` qui va avec.
La balise `<input>` est une balise auto fermante (pas besoinde la fermer)


## Different dossier dans `src`
`assets` dans lequel on va trouver toute les image et police de caractere.
`app` c'est l'application elle meme, avec les modules et des components

## De quoi est composé un component ? 
D'un template = ce que l'on va voir (le visuel sur la page)
D'une feuille de style = police placement etc...
Du JS = qui va permettre de donné des ordres

## Le simple binding 
C'est le fait d'afficher un component

## Le double binding
C'est le fait de modifier le HTML qui lui va modifier le component

## Le ngModel
Permet de faire facilement du double binding mais c'est une mauvaise pratique pour les formulaire.  
![ExmepleAngularNgModel](assets/Exemple-Angular-Instal.PNG)  
Ici on va creer un model, le `[` veux dire input et `(` veux dire output.
Du coup on creer un model qui est en input et en output qui va aller chercher la propriete lastName  
![Exmeple-Angluar-ngModel](assets/Exemple-Angular-Install.PNG)  
Du coup la, la valeur que l'on va avoir dans lastName sera celle que l'on va rentrer en input