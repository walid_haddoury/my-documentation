# Le passage par reference

La memoire vive est tres rapide car elle directement accessible par le CPU, la memoire vive se `reset` quand on eteind l'ordinateur.
Le disque dur (HDD ou SDD) sont plus lent que la memoire vive.

On stock dans le HDD ou SDD et on envoie les infos dont on a besoin dans la memoire vive pour que le programme ce lance plus vite.

Toutes les `primitives` que l'on creer sont toutes stocker au meme endroit dans la memoire vive (primite = number, string, null, true-false, undefined), les prmitives sont dites passé par `copie`
Alors que les `object` sont tous stocker dans une adresse differente

Les primitive vu qu'elle sont passer par copie, on ne peux pas les modifiés ont va modifier un copie et non pas l'original donc on ne verra pas de difference.
Alors que les `object` sont passé par reference donc on peux interagir sur eux, les modifier, etc...  
![exemple1Object](assets/exemplePassageParReference1.PNG)  
![exemple2Object](assets/exemplePassageParReference2.PNG)  
Dans cet exemple on peux modifier l'`object` `pageInfo`, ici le title qui est myAngular ve devenir toto grace au passage par reference de l'object `pageInfo`.  
Alors que si on veux la meme chose sur une `primitive` sa ne marchera pas.


On faire une passage par reference avec les pointeurs :  
![exemplePassageReferencePointeur](assets/exemplePassageReference3.PNG)  
ici pageInfo2 = pageInfo (les deux sont lié dans un sens)
donc ici si on change pageInfo, pageInfo2