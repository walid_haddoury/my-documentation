# Creer un component

Etape de creation d'un component:
- Etre dans le bon dossier (dossier front)
- Ouvrir le terminal et taper `ng g m components/avatar` (ng = angular, g = generate, m = module, components/avatar = creer un dossier components dans lequel est le module (avatar)) on doit creer un module dans lequel on va mettre un component
- Ensuite on tape la commande `ng g c components/avatar` pour creer le component dans le module avatar.
![exmeple1CreationComponent](assets/exempleComponent1.png)  
ici on va dans le dossier `avatar.module.ts` et on ajoute cet ligne pour pouvoir utiliser ce component partout. (on choisis le component que l'on veux exporter)

En Angular on l'interdiction de declarer un component dans plusieurs `Module`, on declare le `component` que dans un `module`

Si l'on veux utiliser notre `Component` on doit importer le `Module` dans le `Module principal` pour pourvoir y acceder !


- Si l'on veux ajouter une image en avatar pour notre `component`:
    - Aller dans le dossier `asset` de src de front
    - Creer un dossier `img` pour y stocker les images et ajouter dedans notre image
    ![exempleAjoutIMG](assets/exempleAjoutIMGComponent.PNG)  
    Ajouter cette ligne dans le fichier `avatar.component.html`.
    

- Si l'on veux avoir un avatar par defaut si on a pas de photo:
    - On va dans le fichier `avatar.component.ts`, on ajoute :  
    ![exempleAjoutAvatarParDefaut](assets/exempleComponentAvatar1.PNG)  
    du coup on a un `input` par defaut  
    ![exempleAfficherAvatarDefaut](assets/exempleComponentAvatarDefaut2.PNG)  
    il faut aller dans le fichier `avatar.component.ts`, on met des crochet autour de `src` pour lui qu'il va devoir aller chercher la valeur de la variable qu'on va lui entrer, et qu'il l'a renvoi sous le meme type que l'original. 
    ensuite on entre `avatarUrl` la on est stocker l'image par defaut.  
    ![exempleAfficherAvatarNew](assets/exempleAvatarComponent3.PNG)  
    Ici dans le `app-avatar` qui est dans dans le `app.component.html` on lui dit que la valeur de `avatarUrl` sera `avatarUrlFromMain` du coup la nouvelle image qu'on a mis
    
Si l'on veux voter pour une image et afficher le nombre de vote:
    - Creer un boutton vote  
    ![exmpmleButtonVote](assets/exempleVoteButtonComponent1.PNG)  
    le `click` permet d'effectuer une action lorsque l'on va cliquer sur le button  
    ![exempleVoteButton2](assets/exempleVoteButtonComponent2.PNG)   
    ![exempleVoteButton3](assets/exempleButtonComponent3.PNG)  
    ![voteEtape1](assets/voteEtape1.PNG)  
    ![voteEtape2](assets/voteEtape2.PNG) 
    faire ça dans `avatar.comporent.ts`
    ![voteEtape3](assets/voteEtape3.PNG)  
    ![voteEtape3](assets/voteEtape4.PNG)  
    le `$event` permet d'aller chercher la valeur qu'on a envoyer avec le `EventEmitter`  
    
