# Le routing

Un main Module est toujours accompagner d'un `main Router module` qui ne contient pas de component, mais seulement des `route` (url que l'on peux utiliser qui vont permettre de charger les autres modules)
![exempleRoute](assets/exempleRoute1.png)  
ici dans notre exemple, nos `route` vont contenir ces informations la.

Dans un `module` de page il n'y a pas de `export`


## Pour creer des `route`:  
- Creer nos `module` et nos `component`
- Creer dans tous nos dossier creer un fichier `(noms du fichier).routing.module.ts`
- Ajouter dans dans ce fichier les infos de `app-routing.module.ts` (faire un copier coller)
- Changer le `forRoot` en `forChild` car il n'y a qu'un père tous les autres sont des enfants
- Changer le noms de la class a exporter `export class` par le noms proposé par WebStorm 
![exempleRouting1](assets/exempleRouting1.png)  

## Pour organiser et declarer nos route:
- On va dans le fichier `app-routing.module.ts` 
- On ajoute `path` permet de creer nos lien (lien internet), les ` '' ` veulent dire qu'on est a la racine
- `component` veux dire qu'on va chercher le component (ici `AppComponent`, le component principal)
- `children` veux dire qu'on a des modules enfants, puis on va les declarer  
![exempleRouting](assets/exempleRouting2.png)  
    on declare les `children` un par un, ont lui donne un `path` (ce que l'on va voir dans le lien lorsque l'on va cliquer sur cette page)
    `loadChildren` `import`, pour charger et importer le module enfant, qui va etre lié au lien, on est obliger de mettre les promesses
    
Dans le `path` si l'on ajoute `:id` on peux allrs attendre un id

## Utiliser nos route
- On va dans le fichier `app.component.html`  
![exempleRoutingLink](assets/exempleRoutingLink1.PNG)  
`[routerLink]` permet de creer un lien qui va aller chercher le `app-route` qui a le meme noms
`<router-outlet>` permet de charger toute les routes et les `children`

## Avoir une page ar defaut
- On retourne dans le fichier `app-routing.module.ts`  
![exempleDefautPage](assets/exempleRoutingDefautPage.png)  
le `redirectTo` permet de dire vers quel route on doit renvoyer
le `pathMatch` est obligatoire c'est une syntaxe obligatoire  


![exmepleLinkPropre](assets/exempleRoutingLink.PNG)  