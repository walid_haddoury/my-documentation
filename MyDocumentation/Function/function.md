 # Les Fonctions 
 
 ````javascript
function randomAction(x, y) {
    if (x < 10 {
        return x * y;
    }
    if (y > 20) {
        return x - y;
        }
}
````

````javascript
function oui(){
    return 'coucou';
}
console.log(oui);
````

````javascript
hostels = hostels.map(transform);

function transform(oui){
    oui.rooms.forEach(room => 
    room.roomName = room.roomName
    .split(' ')
    .map(upName => upName.charAt(0).toUpperCase() + slice(1)
    .join(' ')
    );
    return oui
}
console.log(hostels);
````

## Utliser les `arguments`

````javascript
function testArg(...arg){
    console.log(arg);
}
console.log(testArg(1, 456, 54, 5963));
````

- modifier et interagir avec les arguments
````javascript
function multiply(...arg){
    return arg.reduce((previousValue, currentValue) => previousValue * currentValue)
}
console.log(multiply(20, 63, 9, 1));
````

- Ajouter des argument apres des parametres 
````javascript
function adition(a, b, c, ...arg){
    console.log(a, b, c, arg)
}
adition(4, 5, 3, 99, 100, 964, 5);
````
