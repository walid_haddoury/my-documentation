# My Documentation

## Angular 
- [Services HTTP](./MyDocumentation/Angular/AngularServiceHTTP.md)
- [Création d'un component](./MyDocumentation/Angular/CreerUnComponent.md)
- [Form et Firestore](MyDocumentation/Angular/form&Firestore.md)
- [Installation d'Angular](MyDocumentation/Angular/Installation-d'angular.md)
- [Passage par réference](MyDocumentation/Angular/PassageParReference.md)
- [Principe d'Angular](MyDocumentation/Angular/Principe-d'Angular.md)
- [Le routing](MyDocumentation/Angular/routing.md)

## Arrays
- [Les Arrays](MyDocumentation/Arrays/arrays.md)
- [Array-Map](MyDocumentation/Arrays/Array-Map.md)

## Await Async
- [Await Async](MyDocumentation/Await-Async/await-async.md)

## Bootstrap et CSS
- [Bootstrap](MyDocumentation/Bootstrap/Bootstrap.md)
- [Bootstrap et le CSS](MyDocumentation/Bootstrap%20et%20CSS/BootstrapEtCSS.md)

## La boucle FOR
- [For](MyDocumentation/Boucle%20For/for.md)

## Les class
- [Les class](MyDocumentation/Class/Les-Classes.md)

## Les promesse
- [Création de promesse](MyDocumentation/Creer%20une%20promesse/CreateAPromise.md)

## Firebase
- [Installation Firebase](MyDocumentation/Firebase/FireBase-Install.md)
- [TypeScript](MyDocumentation/Firebase/TypeScript.md)

## Firestore
- [Installation de FIrestore](MyDocumentation/Firestore/install-firestore.md)

## Functions
- [Les functions](MyDocumentation/Function/function.md)

## Git Flow
- [Le Git flow](MyDocumentation/Git%20Flow/Git-Flow.md)

## Herbergement
- [L'hebergement](MyDocumentation/Hebergement/herbergement.md)

## Markdown
- [Le Markdown](MyDocumentation/Markdown/Markdown.md)

## Node
- [Installation de Node et le CRUD](MyDocumentation/Node/CRUD.md)
- [Opérateur du CRUD](MyDocumentation/Node/get-delete-post-put.md)
- [Opérateur Patch](MyDocumentation/Node/patch.md)
- [Query Collection](MyDocumentation/Node/Query-Collection.md)

## Object
- [Fonctionnement des Objects](MyDocumentation/Object/Object.keys&Object.value.md)

## Promise All
- [Le Promise.all](MyDocumentation/Promise%20All/promise.all.md)

## Random
- [Opérateur Random](MyDocumentation/Random/Random.md)

## React
- [React, install et Virtual DOM](MyDocumentation/React/React&VirtualDOM.md)

## Truthy & Falsy
- [Opérateur Truthy & Falsy](MyDocumentation/Truthy%20&%20Falsy/truthy&Falsy.md)

## Quick Note Angular
- [Installation Angular](MyDocumentation/Quick-Note-Angular/installation-angular.md)
- [Initialisation d'une base de donnée](MyDocumentation/Quick-Note-Angular/installer-la-base-de-donnés.md)
- [Le routing entre deux Module](MyDocumentation/Quick-Note-Angular/routing-between-two-children.md)

## Quick Note Back-End
- [Installation Node et du Back-End](MyDocumentation/Quick-note-BackEnd/install-new-project.md)66